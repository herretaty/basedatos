package com.tatiana.fruteria;

import com.tatiana.fruteria.base.FrutaVerdura;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {

    private JFrame frame;
    private JPanel panel1;
    private JTextField nombreTxT;
    private JTextField tipoTxT;
    private JTextField origenTxT;
    private JTextField kiloTxT;
    private JTextField precioTxT;
    private JLabel lblFrutaVerdura;
    private JButton altaFrutaVerduraBtn;
    private JButton mostrarFrutaVerduraBtn;
    private JComboBox comboBox;


    private LinkedList<FrutaVerdura> lista;
    private DefaultComboBoxModel<FrutaVerdura> dcbm;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);

        altaFrutaVerduraBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaFrutaVerdura(nombreTxT.getText(),tipoTxT.getText(),origenTxT.getText(),kiloTxT.getText(),precioTxT.getText());
                refrescarComboBox();
            }
        });
        mostrarFrutaVerduraBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrutaVerdura seleccionado = (FrutaVerdura) dcbm.getSelectedItem();
                lblFrutaVerdura.setText(seleccionado.toString());
            }
        });
    }
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (FrutaVerdura frutaVerdura: lista) {
            dcbm.addElement(frutaVerdura);
        }
    }

    public static void main(String[] args) {

        Vista vista = new Vista();
    }

    private void altaFrutaVerdura(String nombre, String tipo, String origen, String kilo, String precio) {
        lista.add(new FrutaVerdura(nombre, tipo, origen, kilo, precio));
    }

    private void crearMenu() {
        JMenuBar barra= new JMenuBar();
        JMenu menu= new JMenu("Archivo");
        JMenuItem itemExportarXML= new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML= new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }

        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero= selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList frutaVerduras = documento.getElementsByTagName("frutaVerdura");
            for (int i = 0; i < frutaVerduras.getLength(); i++) {
                Node frutaVerdura = frutaVerduras.item(i);
                Element elemento = (Element) frutaVerdura;

                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String tipo = elemento.getElementsByTagName("tipo").item(0).getChildNodes().item(0).getNodeValue();
                String origen = elemento.getElementsByTagName("origen").item(0).getChildNodes().item(0).getNodeValue();
                String kilo = elemento.getElementsByTagName("kilo").item(0).getChildNodes().item(0).getNodeValue();
                String precio = elemento.getElementsByTagName("precio").item(0).getChildNodes().item(0).getNodeValue();

                altaFrutaVerdura(nombre,tipo,origen,kilo,precio);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
    private void exportarXML (File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            Document documento = dom.createDocument(null, "xml", null);

            Element raiz = documento.createElement("frutaVerdura");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoFrutaVerdura;
            Element nodoDatos;
            Text dato;

            for (FrutaVerdura frutaVerdura : lista) {
                nodoFrutaVerdura = documento.createElement("frutaVerdura");
                raiz.appendChild(nodoFrutaVerdura);

                nodoDatos = documento.createElement("nombre");
                nodoFrutaVerdura.appendChild(nodoDatos);

                dato = documento.createTextNode(frutaVerdura.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("tipo");
                nodoFrutaVerdura.appendChild(nodoDatos);

                dato = documento.createTextNode(frutaVerdura.getTipo());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("origen");
                nodoFrutaVerdura.appendChild(nodoDatos);

                dato = documento.createTextNode(frutaVerdura.getOrigen());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("kilo");
                nodoFrutaVerdura.appendChild(nodoDatos);

                dato = documento.createTextNode(frutaVerdura.getKilo());
                nodoDatos.appendChild(dato);

                nodoDatos= documento.createElement("precio");
                nodoFrutaVerdura.appendChild(nodoDatos);

                dato=documento.createTextNode(frutaVerdura.getPrecio());
                nodoDatos.appendChild(dato);

            }

            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }


}

