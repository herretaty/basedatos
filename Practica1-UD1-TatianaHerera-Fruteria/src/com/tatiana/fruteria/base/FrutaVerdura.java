package com.tatiana.fruteria.base;

public class FrutaVerdura {
    String nombre;
    String tipo;
    String origen;
    String kilo;
    String precio;

    public FrutaVerdura(String nombre, String tipo, String origen, String kilo, String precio) {
        this.nombre=nombre;
        this.tipo=tipo;
        this.origen=origen;
        this.kilo=kilo;
        this.precio=precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getKilo() {
        return kilo;
    }

    public void setKilo(String kilo) {
        this.kilo = kilo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }



    @Override
    public String toString() {
        return "FrutaVerdura{" +
                "nombre='" + nombre+ '\'' +
                ", tipo='" + tipo+ '\'' +
                ", origen='" + origen+ '\'' +
                ", kilo='" + kilo+ '\'' +
                ", precio='" + precio+ '\'' +
                '}';
    }
}
